import random
from ipaddress import IPv4Address, IPv6Address


def random_ip():
    return random.choice([random_ipv4(), random_ipv6()])


def random_ipv4():
    bits = random.getrandbits(32)
    address = IPv4Address(bits)
    return str(address)


def random_ipv6():
    bits = random.getrandbits(128)
    address = IPv6Address(bits)
    return str(address)
