from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

app_name = "api"
urlpatterns = [
    url(r"^$", views.api_root, name="root"),
    url(r"^is_server_up/", views.IsServerUp.as_view(), name="is_server_up"),
    url(r"^is_db_up/", views.IsDatabaseUp.as_view(), name="is_db_up"),
    url(
        r"^percentage_disk_space_remaining/",
        views.GetPercentageDiskSpaceRemaining.as_view(),
        name="percentage_disk_space_remaining",
    ),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json", "html"])
