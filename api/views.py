from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from shutil import disk_usage
import psycopg2

from django.conf import settings


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "is_server_up": reverse("api:is_server_up", request=request, format=format),
            "is_db_up": reverse("api:is_db_up", request=request, format=format),
            "percentage_disk_space_remaining": reverse(
                "api:percentage_disk_space_remaining", request=request, format=format
            ),
        }
    )


class IsServerUp(APIView):
    def get(self, request, format=None):

        data = {"ServerUp": True, "DebugModeEnabled": settings.DEBUG}

        return Response(data)


class IsDatabaseUp(APIView):
    def get(self, request, format=None):

        try:
            conn = psycopg2.connect(
                f"dbname='{settings.DATABASES['default']['NAME']}' user='{settings.DATABASES['default']['USER']}' host='{settings.DATABASES['default']['HOST']}' password='' connect_timeout=1 "
            )
            conn.close()
            return Response({"DatabaseIsUp": True})
        except:
            return Response({"DatabaseIsUp": False})


class GetPercentageDiskSpaceRemaining(APIView):
    def get(self, request, format=None):

        total, used, free = disk_usage("/")
        percent_space_remaining = round(free / total, 2)

        return Response({"PercentageDiskSpaceRemaining": percent_space_remaining})
