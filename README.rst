InnoPolls Challenge
==============

to run::

    git clone https://gitlab.com/gitlab_phil/innogameschallenge.git
    cd innogameschallenge
    docker-compose up -d
    docker-compose run web python manage.py makemigrations
    docker-compose run web python manage.py migrate
    docker-compose run web python manage.py test polls
    docker-compose run web python manage.py generate_dummy_data

go to::

    http://localhost:8000/polls
    http://localhost:8000/admin
    http://localhost:8000/api

login with::

    user: admin
    pass: password
