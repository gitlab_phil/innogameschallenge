from django.contrib.admin import AdminSite


class InnoAdminSite(AdminSite):
    site_header = "InnoPolls administration"


admin_site = InnoAdminSite(name="InnoAdmin")
