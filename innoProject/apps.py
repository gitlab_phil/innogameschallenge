from django.contrib.admin.apps import AdminConfig


class InnoAdminConfig(AdminConfig):
    default_site = "innoProject.admin.InnoAdminSite"
