import datetime
import uuid

from django.db import models
from django.utils import timezone


from django.db.models import GenericIPAddressField
from django.db.models import Lookup


@GenericIPAddressField.register_lookup
class IsContainedByOrEquals(Lookup):
    lookup_name = "iscontainedbyorequals"

    def as_postgresql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return "%s <<= inet %s" % (lhs, rhs), params


class Author(models.Model):
    name = models.CharField(max_length=128)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    ip = models.GenericIPAddressField(protocol="both", null=True, blank=True)


class Question(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published", default=timezone.now)

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    was_published_recently.admin_order_field = "pub_date"
    was_published_recently.boolean = True
    was_published_recently.short_description = "Published recently?"


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
