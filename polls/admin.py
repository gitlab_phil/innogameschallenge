from django.contrib import admin
from ipaddress import ip_address, ip_network

from .models import Question, Choice, Author


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {"fields": ["question_text"]}),
        ("Date information", {"fields": ["pub_date"], "classes": ["collapse"]}),
    ]
    inlines = [ChoiceInline]
    list_display = ("question_text", "pub_date", "was_published_recently")
    list_filter = ["pub_date"]
    search_fields = ["question_text"]


class QuestionInline(admin.StackedInline):
    model = Question
    extra = 1


class AuthorAdmin(admin.ModelAdmin):
    fieldsets = [("Author", {"fields": ["name", "ip"]})]
    list_display = ["masked_ip", "uuid", "name"]
    inlines = [QuestionInline]

    def masked_ip(self, obj):
        try:
            ip = ip_address(obj.ip)
        except ValueError:
            pass
        else:
            ip_str = ip_address(obj.ip).exploded
            if ip.version == 4:
                return ip_str[: ip_str.rfind(".")] + ".***"
            if ip.version == 6:
                return ip_str[: ip_str.rfind(":")] + ":****"

    masked_ip.short_description = "IP"

    search_fields = ("ip",)

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(
            request, queryset, search_term
        )
        try:
            ip_network(search_term)
        except ValueError:
            pass
        else:
            # uses the custom lookup defined in models.py
            queryset |= self.model.objects.filter(ip__iscontainedbyorequals=search_term)
        return queryset, use_distinct


admin.site.register(Author, AuthorAdmin)
admin.site.register(Question, QuestionAdmin)
