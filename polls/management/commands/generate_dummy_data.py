from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string, random
from polls.models import Author, Question, Choice
from libs.ip_address import random_ip


class Command(BaseCommand):
    help = "Generates dummy data including a dummy superuser - this is not safe - only use if you are a dummy"
    number_of_authors = 20
    number_of_polls = 20
    number_of_choices_per_question = 3

    _authors = None
    _question = None

    _super_user = "admin"
    _super_email = "admin@myproject.com"
    _super_pass = "password"

    def add_arguments(self, parser):
        parser.add_argument(
            "-authors",
            type=int,
            default=self.number_of_authors,
            help="How many authors will be created?",
        )
        parser.add_argument(
            "-polls",
            type=int,
            default=self.number_of_polls,
            help="How many polls will be created?",
        )

    def handle(self, *args, **kwargs):
        self.number_of_authors = kwargs["authors"]
        self.number_of_polls = kwargs["polls"]

        self.create_superuser()
        self.create_authors()
        self.create_polls()

    def create_superuser(self):
        user = get_user_model()
        user.objects.create_superuser(
            self._super_user, self._super_email, self._super_pass
        )

    def create_authors(self):
        for i in range(self.number_of_authors):
            self.create_author()

    @staticmethod
    def create_author():
        Author.objects.create(name=get_random_string(12), ip=random_ip())

    def get_random_author_id(self):
        return random.randint(1, self.number_of_authors)

    def create_polls(self):
        for i in range(self.number_of_polls):
            self.create_question()
            self.create_choices()

    def create_question(self):
        self._question = Question.objects.create(
            author_id=self.get_random_author_id(),
            question_text=f"{get_random_string(30)}?",
        )

    def create_choices(self):
        for i in range(self.number_of_choices_per_question):
            self.create_choice()

    def create_choice(self):
        Choice.objects.create(
            question=self._question,
            choice_text=get_random_string(),
            votes=random.randint(0, 10),
        )
